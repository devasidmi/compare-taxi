package ru.vasidmi.comparetaxi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vasidmi.comparetaxi.Database;
import ru.vasidmi.comparetaxi.R;
import ru.vasidmi.comparetaxi.adapters.HistoryAdapter;

public class HistoryFragment extends Fragment {

    @BindView(R.id.taxi_list)
    RecyclerView mRecyclerView;

    public static final String TAG = "HistoryFragment";

    private View mView;
    private Context mContext;
    private Database mDatabase;
    private HistoryAdapter mHistoryAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getContext();
        mView = LayoutInflater.from(mContext).inflate(R.layout.list_fragment, container, false);
        ButterKnife.bind(this, mView);
        mDatabase = new Database(getActivity());
        loadHistory();
        return mView;
    }

    private void loadHistory() {
        mHistoryAdapter = new HistoryAdapter(mContext, mDatabase.getSearchValues());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mHistoryAdapter);
    }
}
