package ru.vasidmi.comparetaxi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arasthel.asyncjob.AsyncJob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.vasidmi.comparetaxi.R;
import ru.vasidmi.comparetaxi.adapters.TaxiAdapter;
import ru.vasidmi.comparetaxi.apis.KlassApiService;
import ru.vasidmi.comparetaxi.apis.RitmApiService;
import ru.vasidmi.comparetaxi.apis.UberApiService;
import ru.vasidmi.comparetaxi.apis.YandexApiService;
import ru.vasidmi.comparetaxi.models.Klass.KlassModel;
import ru.vasidmi.comparetaxi.models.Klass.Tariff;
import ru.vasidmi.comparetaxi.models.Klass.TokenModel;
import ru.vasidmi.comparetaxi.models.Ritm.RitmModel;
import ru.vasidmi.comparetaxi.models.Route;
import ru.vasidmi.comparetaxi.models.Taxi;
import ru.vasidmi.comparetaxi.models.Uber.Price;
import ru.vasidmi.comparetaxi.models.Uber.UberModel;
import ru.vasidmi.comparetaxi.models.Yandex.Option;
import ru.vasidmi.comparetaxi.models.Yandex.YandexModel;

public class TaxiNearFragment extends Fragment {

    public static final String TAG = "TaxiNearFragment";
    private Context mContext;
    private View mView;
    @BindView(R.id.taxi_list)
    RecyclerView mRecyclerView;
    private TaxiAdapter mTaxiAdapter;
    private Route mRoute;
    private KlassApiService mKlassApiService;

    String q_token;
    String q;
    String lang;
    String apikey;
    String country;
    String city;
    String precision;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getContext();
        mView = LayoutInflater.from(mContext).inflate(R.layout.list_fragment, container, false);
        ButterKnife.bind(this, mView);
        setupKlassParams();
        mKlassApiService = KlassApiService.retrofit.create(KlassApiService.class);
        mTaxiAdapter = new TaxiAdapter(mContext, new ArrayList<Taxi>());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mTaxiAdapter);
        mRoute = (Route) getArguments().get("route");
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {
                getUber();
                getYandex();
                getRitm();
                startKlass();
            }
        });

        return mView;
    }

    private void setupKlassParams() {
        q_token = mContext.getResources().getString(R.string.q_token);
        q = mContext.getResources().getString(R.string.q);
        lang = mContext.getResources().getString(R.string.lang);
        apikey = mContext.getResources().getString(R.string.apikey);
        country = mContext.getResources().getString(R.string.country);
        city = mContext.getResources().getString(R.string.city);
        precision = mContext.getResources().getString(R.string.precision);
    }

    public static TaxiNearFragment newInstance(Route mRoute) {

        Bundle args = new Bundle();
        args.putSerializable("route", mRoute);
        TaxiNearFragment fragment = new TaxiNearFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void getUber() {
        UberApiService mUberApiService = UberApiService.retrofit.create(UberApiService.class);
        Call<UberModel> call = mUberApiService.getUber(mContext.getResources().getString(R.string.uber_token),
                mRoute.getStartLat(), mRoute.getStartLng(),
                mRoute.getEndLat(), mRoute.getEndLng());
        call.enqueue(new Callback<UberModel>() {
            @Override
            public void onResponse(@NonNull Call<UberModel> call, @NonNull final Response<UberModel> response) {
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        if (response.body() != null) {
                            for (Price price : response.body().getPrices()) {
                                mTaxiAdapter.addTaxi(new Taxi("uber", price.getLocalizedDisplayName(), price.getHighEstimate(), ""));
                                mRecyclerView.smoothScrollToPosition(0);
                            }
                        }
                    }
                });

            }

            @Override
            public void onFailure(@NonNull Call<UberModel> call, @NonNull Throwable t) {
                Log.e("YAY!", "uber error");
            }
        });
    }

    private void getYandex() {
        YandexApiService mYandexApiService = YandexApiService.retrofit.create(YandexApiService.class);
        String clid = mContext.getResources().getString(R.string.clid);
        String yandexApiKey = mContext.getResources().getString(R.string.yandex_api_key);
        String class_ = mContext.getResources().getString(R.string.class_all);
        String rll = String.format("%s,%s~%s,%s", String.valueOf(mRoute.getStartLng()), String.valueOf(mRoute.getStartLat()),
                String.valueOf(mRoute.getEndLng()), String.valueOf(mRoute.getEndLat()));
        Call<YandexModel> call = mYandexApiService.getYandex(clid, yandexApiKey, rll, class_);
        call.enqueue(new Callback<YandexModel>() {
            @Override
            public void onResponse(@NonNull Call<YandexModel> call, @NonNull final Response<YandexModel> response) {
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        if (response.body() != null) {
                            for (Option o : response.body().getOptions()) {
                                mTaxiAdapter.addTaxi(new Taxi("yandex", "Яндекс Такси", o.getPrice(), o.getClassText()));
                                mRecyclerView.smoothScrollToPosition(0);
                            }
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call<YandexModel> call, Throwable t) {
                Log.e("YAY!", "yandex error");
            }
        });
    }

    private void getRitm() {
        RitmApiService mRitmApiService = RitmApiService.retrofit.create(RitmApiService.class);
        RequestBody a = RequestBody.create(MediaType.parse("text/plain"), String.format("%s,%s", String.valueOf(mRoute.getStartLat()), String.valueOf(mRoute.getStartLng())));
        RequestBody b = RequestBody.create(MediaType.parse("text/plain"), String.format("%s,%s", String.valueOf(mRoute.getEndLat()), String.valueOf(mRoute.getEndLng())));
        RequestBody time = RequestBody.create(MediaType.parse("text/plain"), "now");
        RequestBody tarif = RequestBody.create(MediaType.parse("text/plain"), "7");
        RequestBody calc = RequestBody.create(MediaType.parse("text/plain"), "Y");
        RequestBody an = RequestBody.create(MediaType.parse("text/plain"), "from");
        RequestBody bn = RequestBody.create(MediaType.parse("text/plain"), "to");

        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("ORDER_POINT_A_GEO", a);
        requestBodyMap.put("ORDER_POINT_B_GEO", b);
        requestBodyMap.put("TIME_TYPE", time);
        requestBodyMap.put("TARIF", tarif);
        requestBodyMap.put("CALCULATE", calc);
        requestBodyMap.put("ORDER_POINT_A", an);
        requestBodyMap.put("ORDER_POINT_B", bn);
        Call<RitmModel> call = mRitmApiService.getRitm(requestBodyMap);
        call.enqueue(new Callback<RitmModel>() {
            @Override
            public void onResponse(@NonNull Call<RitmModel> call, @NonNull final Response<RitmModel> response) {
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        if (response.body().getSum() != null) {
                            mTaxiAdapter.addTaxi(new Taxi("ritm", "Такси Ритм", response.body().getSum(), ""));
                            mRecyclerView.smoothScrollToPosition(0);
                        }


                    }
                });
            }

            @Override
            public void onFailure(@NonNull Call<RitmModel> call, @NonNull Throwable t) {
                Log.e("YAY!", "ritm error");
            }
        });
    }

    private void getKlassToken() {
        Call<TokenModel> call = mKlassApiService.getToken(q_token, lang, apikey);
        call.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                if (response.body() != null && response.body().getSuccess())
                    getKlass(response.body().getToken());
            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                Log.e("YAY!", "klass error");
            }
        });
    }

    private void getKlass(String stoken) {
        Call<KlassModel> call = mKlassApiService.getKlass(q, country, city, mRoute.getStartTitle(), precision, country, city, mRoute.getEndTitle(), precision, lang, apikey, stoken);
        call.enqueue(new Callback<KlassModel>() {
            @Override
            public void onResponse(Call<KlassModel> call, Response<KlassModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    for (Tariff t : response.body().getTariffs()) {
                        mTaxiAdapter.addTaxi(new Taxi("klass", "Такси Класс", t.getCostFull().intValue(), t.getName()));
                        mRecyclerView.smoothScrollToPosition(0);
                    }
                }
            }

            @Override
            public void onFailure(Call<KlassModel> call, Throwable t) {
                Log.e("YAY!", "klass error");
            }
        });
    }

    private void startKlass() {
        getKlassToken();
    }
}
