package ru.vasidmi.comparetaxi.models.Ritm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("amountForExtras")
    @Expose
    private Integer amountForExtras;
    @SerializedName("transfer")
    @Expose
    private String transfer;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    /**
     * No args constructor for use in serialization
     */
    public Data() {
    }

    /**
     * @param amount
     * @param transfer
     * @param details
     * @param amountForExtras
     */
    public Data(Integer amount, Integer amountForExtras, String transfer, List<Detail> details) {
        super();
        this.amount = amount;
        this.amountForExtras = amountForExtras;
        this.transfer = transfer;
        this.details = details;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmountForExtras() {
        return amountForExtras;
    }

    public void setAmountForExtras(Integer amountForExtras) {
        this.amountForExtras = amountForExtras;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

}