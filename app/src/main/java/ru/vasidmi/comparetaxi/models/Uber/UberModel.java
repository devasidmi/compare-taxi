package ru.vasidmi.comparetaxi.models.Uber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UberModel {

    @SerializedName("prices")
    @Expose
    private List<Price> prices = null;

    /**
     * No args constructor for use in serialization
     */
    public UberModel() {
    }

    /**
     * @param prices
     */
    public UberModel(List<Price> prices) {
        super();
        this.prices = prices;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

}
