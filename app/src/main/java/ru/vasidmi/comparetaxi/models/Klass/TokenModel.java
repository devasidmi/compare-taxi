package ru.vasidmi.comparetaxi.models.Klass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenModel {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("Success")
    @Expose
    private Boolean success;

    /**
     * No args constructor for use in serialization
     */
    public TokenModel() {
    }

    /**
     * @param token
     * @param success
     */
    public TokenModel(String token, Boolean success) {
        super();
        this.token = token;
        this.success = success;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}


