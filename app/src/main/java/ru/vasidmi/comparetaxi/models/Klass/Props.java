package ru.vasidmi.comparetaxi.models.Klass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Props {

    @SerializedName("Category")
    @Expose
    private Category category;
    @SerializedName("Type")
    @Expose
    private Type type;
    @SerializedName("Properties")
    @Expose
    private List<Property> properties = null;

    /**
     * No args constructor for use in serialization
     */
    public Props() {
    }

    /**
     * @param category
     * @param properties
     * @param type
     */
    public Props(Category category, Type type, List<Property> properties) {
        super();
        this.category = category;
        this.type = type;
        this.properties = properties;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

}
