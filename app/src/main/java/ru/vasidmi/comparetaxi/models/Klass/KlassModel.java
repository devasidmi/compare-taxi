package ru.vasidmi.comparetaxi.models.Klass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KlassModel {

    @SerializedName("Tariffs")
    @Expose
    private List<Tariff> tariffs = null;
    @SerializedName("Duration")
    @Expose
    private Integer duration;
    @SerializedName("Distance")
    @Expose
    private Integer distance;
    @SerializedName("TrafficJamsAvoided")
    @Expose
    private Boolean trafficJamsAvoided;
    @SerializedName("Addresses")
    @Expose
    private List<Address> addresses = null;
    @SerializedName("Success")
    @Expose
    private Boolean success;

    /**
     * No args constructor for use in serialization
     */
    public KlassModel() {
    }

    /**
     * @param tariffs
     * @param distance
     * @param duration
     * @param trafficJamsAvoided
     * @param addresses
     * @param success
     */
    public KlassModel(List<Tariff> tariffs, Integer duration, Integer distance, Boolean trafficJamsAvoided, List<Address> addresses, Boolean success) {
        super();
        this.tariffs = tariffs;
        this.duration = duration;
        this.distance = distance;
        this.trafficJamsAvoided = trafficJamsAvoided;
        this.addresses = addresses;
        this.success = success;
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<Tariff> tariffs) {
        this.tariffs = tariffs;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Boolean getTrafficJamsAvoided() {
        return trafficJamsAvoided;
    }

    public void setTrafficJamsAvoided(Boolean trafficJamsAvoided) {
        this.trafficJamsAvoided = trafficJamsAvoided;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
