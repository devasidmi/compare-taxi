package ru.vasidmi.comparetaxi.models.Yandex;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YandexModel {

    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("distance")
    @Expose
    private Float distance;
    @SerializedName("options")
    @Expose
    private List<Option> options = null;
    @SerializedName("time")
    @Expose
    private Float time;

    /**
     * No args constructor for use in serialization
     */
    public YandexModel() {
    }

    /**
     * @param time
     * @param distance
     * @param options
     * @param currency
     */
    public YandexModel(String currency, Float distance, List<Option> options, Float time) {
        super();
        this.currency = currency;
        this.distance = distance;
        this.options = options;
        this.time = time;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public Float getTime() {
        return time;
    }

    public void setTime(Float time) {
        this.time = time;
    }

}
