package ru.vasidmi.comparetaxi.models.Ritm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("id_zone")
    @Expose
    private String idZone;
    @SerializedName("name_zone")
    @Expose
    private String nameZone;
    @SerializedName("time")
    @Expose
    private Float time;
    @SerializedName("distance")
    @Expose
    private Float distance;

    /**
     * No args constructor for use in serialization
     */
    public Detail() {
    }

    /**
     * @param distance
     * @param time
     * @param nameZone
     * @param idZone
     */
    public Detail(String idZone, String nameZone, Float time, Float distance) {
        super();
        this.idZone = idZone;
        this.nameZone = nameZone;
        this.time = time;
        this.distance = distance;
    }

    public String getIdZone() {
        return idZone;
    }

    public void setIdZone(String idZone) {
        this.idZone = idZone;
    }

    public String getNameZone() {
        return nameZone;
    }

    public void setNameZone(String nameZone) {
        this.nameZone = nameZone;
    }

    public Float getTime() {
        return time;
    }

    public void setTime(Float time) {
        this.time = time;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

}
