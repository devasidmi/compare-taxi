package ru.vasidmi.comparetaxi.models.Uber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("localized_display_name")
    @Expose
    private String localizedDisplayName;
    @SerializedName("distance")
    @Expose
    private Float distance;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("high_estimate")
    @Expose
    private Integer highEstimate;
    @SerializedName("low_estimate")
    @Expose
    private Integer lowEstimate;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("estimate")
    @Expose
    private String estimate;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;

    /**
     * No args constructor for use in serialization
     */
    public Price() {
    }

    /**
     * @param duration
     * @param distance
     * @param currencyCode
     * @param localizedDisplayName
     * @param lowEstimate
     * @param highEstimate
     * @param displayName
     * @param estimate
     * @param productId
     */
    public Price(String localizedDisplayName, Float distance, String displayName, String productId, Integer highEstimate, Integer lowEstimate, Integer duration, String estimate, String currencyCode) {
        super();
        this.localizedDisplayName = localizedDisplayName;
        this.distance = distance;
        this.displayName = displayName;
        this.productId = productId;
        this.highEstimate = highEstimate;
        this.lowEstimate = lowEstimate;
        this.duration = duration;
        this.estimate = estimate;
        this.currencyCode = currencyCode;
    }

    public String getLocalizedDisplayName() {
        return localizedDisplayName;
    }

    public void setLocalizedDisplayName(String localizedDisplayName) {
        this.localizedDisplayName = localizedDisplayName;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getHighEstimate() {
        return highEstimate;
    }

    public void setHighEstimate(Integer highEstimate) {
        this.highEstimate = highEstimate;
    }

    public Integer getLowEstimate() {
        return lowEstimate;
    }

    public void setLowEstimate(Integer lowEstimate) {
        this.lowEstimate = lowEstimate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}