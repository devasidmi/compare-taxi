package ru.vasidmi.comparetaxi.models;

public class Taxi {
    private String company;
    private String title;
    private int price;
    private String comment1;

    public Taxi(String company, String title, int price, String comment1) {
        this.company = company;
        this.title = title;
        this.price = price;
        this.comment1 = comment1;
    }


    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getComment1() {
        return comment1;
    }

    public void setComment1(String comment1) {
        this.comment1 = comment1;
    }
}
