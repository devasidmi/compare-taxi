package ru.vasidmi.comparetaxi.models.Ritm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RitmModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("out_data")
    @Expose
    private OutData outData;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("sum")
    @Expose
    private Integer sum;
    @SerializedName("text")
    @Expose
    private String text;

    /**
     * No args constructor for use in serialization
     */
    public RitmModel() {
    }

    /**
     * @param text
     * @param status
     * @param data
     * @param sum
     * @param outData
     */
    public RitmModel(String status, OutData outData, Data data, Integer sum, String text) {
        super();
        this.status = status;
        this.outData = outData;
        this.data = data;
        this.sum = sum;
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OutData getOutData() {
        return outData;
    }

    public void setOutData(OutData outData) {
        this.outData = outData;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
