package ru.vasidmi.comparetaxi.models.Klass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Type {

    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Name")
    @Expose
    private String name;

    /**
     * No args constructor for use in serialization
     */
    public Type() {
    }

    /**
     * @param name
     * @param iD
     */
    public Type(Integer iD, String name) {
        super();
        this.iD = iD;
        this.name = name;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
