package ru.vasidmi.comparetaxi.models.Klass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Property {

    @SerializedName("Key")
    @Expose
    private String key;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Required")
    @Expose
    private Boolean required;
    @SerializedName("Value")
    @Expose
    private Value value;
    @SerializedName("PropertyType")
    @Expose
    private String propertyType;
    @SerializedName("SequenceNumber")
    @Expose
    private Integer sequenceNumber;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Name")
    @Expose
    private String name;

    /**
     * No args constructor for use in serialization
     */
    public Property() {
    }

    /**
     * @param propertyType
     * @param name
     * @param value
     * @param required
     * @param type
     * @param iD
     * @param sequenceNumber
     * @param key
     */
    public Property(String key, String type, Boolean required, Value value, String propertyType, Integer sequenceNumber, Integer iD, String name) {
        super();
        this.key = key;
        this.type = type;
        this.required = required;
        this.value = value;
        this.propertyType = propertyType;
        this.sequenceNumber = sequenceNumber;
        this.iD = iD;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
