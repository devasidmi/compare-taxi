package ru.vasidmi.comparetaxi.models.Klass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CountryName")
    @Expose
    private String countryName;
    @SerializedName("CityName")
    @Expose
    private String cityName;
    @SerializedName("LocalityName")
    @Expose
    private String localityName;
    @SerializedName("PostalCode")
    @Expose
    private String postalCode;
    @SerializedName("StreetName")
    @Expose
    private String streetName;
    @SerializedName("ObjectName")
    @Expose
    private String objectName;
    @SerializedName("HouseNumber")
    @Expose
    private String houseNumber;
    @SerializedName("ApartmentNumber")
    @Expose
    private String apartmentNumber;
    @SerializedName("EntranceNumber")
    @Expose
    private Integer entranceNumber;
    @SerializedName("GeoPoint")
    @Expose
    private GeoPoint geoPoint;
    @SerializedName("CoordinatesOnly")
    @Expose
    private Boolean coordinatesOnly;
    @SerializedName("Precision")
    @Expose
    private Integer precision;

    /**
     * No args constructor for use in serialization
     */
    public Address() {
    }

    /**
     * @param countryName
     * @param localityName
     * @param precision
     * @param entranceNumber
     * @param objectName
     * @param apartmentNumber
     * @param cityName
     * @param streetName
     * @param houseNumber
     * @param id
     * @param geoPoint
     * @param postalCode
     * @param coordinatesOnly
     */
    public Address(Integer id, String countryName, String cityName, String localityName, String postalCode, String streetName, String objectName, String houseNumber, String apartmentNumber, Integer entranceNumber, GeoPoint geoPoint, Boolean coordinatesOnly, Integer precision) {
        super();
        this.id = id;
        this.countryName = countryName;
        this.cityName = cityName;
        this.localityName = localityName;
        this.postalCode = postalCode;
        this.streetName = streetName;
        this.objectName = objectName;
        this.houseNumber = houseNumber;
        this.apartmentNumber = apartmentNumber;
        this.entranceNumber = entranceNumber;
        this.geoPoint = geoPoint;
        this.coordinatesOnly = coordinatesOnly;
        this.precision = precision;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getLocalityName() {
        return localityName;
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public Integer getEntranceNumber() {
        return entranceNumber;
    }

    public void setEntranceNumber(Integer entranceNumber) {
        this.entranceNumber = entranceNumber;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public Boolean getCoordinatesOnly() {
        return coordinatesOnly;
    }

    public void setCoordinatesOnly(Boolean coordinatesOnly) {
        this.coordinatesOnly = coordinatesOnly;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

}