package ru.vasidmi.comparetaxi.models;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class Route implements Serializable {

    private double startLat;
    private double startLng;
    private double endLat;
    private double endLng;
    private String startTitle;
    private String endTitle;

    public Route() {
    }

    public LatLng getStartCoords() {
        return new LatLng(startLat, startLng);
    }

    public LatLng getEndCoords() {
        return new LatLng(endLat, endLng);
    }

    public void setStartCoords(LatLng coords) {
        this.startLat = coords.latitude;
        this.startLng = coords.longitude;
    }

    public void setEndCoords(LatLng coords) {
        this.endLat = coords.latitude;
        this.endLng = coords.longitude;
    }

    public double getStartLat() {
        return startLat;
    }

    public double getStartLng() {
        return startLng;
    }

    public double getEndLat() {
        return endLat;
    }

    public double getEndLng() {
        return endLng;
    }

    public String getStartTitle() {
        return startTitle;
    }

    public void setStartTitle(String startTitle) {
        this.startTitle = startTitle;
    }

    public String getEndTitle() {
        return endTitle;
    }

    public void setEndTitle(String endTitle) {
        this.endTitle = endTitle;
    }
}
