package ru.vasidmi.comparetaxi.models.Ritm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutData {

    @SerializedName("body")
    @Expose
    private String body;

    /**
     * No args constructor for use in serialization
     */
    public OutData() {
    }

    /**
     * @param body
     */
    public OutData(String body) {
        super();
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

}
