package ru.vasidmi.comparetaxi.models.Yandex;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Option {

    @SerializedName("class_level")
    @Expose
    private Integer classLevel;
    @SerializedName("class_name")
    @Expose
    private String className;
    @SerializedName("class_text")
    @Expose
    private String classText;
    @SerializedName("min_price")
    @Expose
    private Integer minPrice;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("price_text")
    @Expose
    private String priceText;
    @SerializedName("waiting_time")
    @Expose
    private Float waitingTime;

    /**
     * No args constructor for use in serialization
     */
    public Option() {
    }

    /**
     * @param waitingTime
     * @param price
     * @param priceText
     * @param classLevel
     * @param classText
     * @param className
     * @param minPrice
     */
    public Option(Integer classLevel, String className, String classText, Integer minPrice, Integer price, String priceText, Float waitingTime) {
        super();
        this.classLevel = classLevel;
        this.className = className;
        this.classText = classText;
        this.minPrice = minPrice;
        this.price = price;
        this.priceText = priceText;
        this.waitingTime = waitingTime;
    }

    public Integer getClassLevel() {
        return classLevel;
    }

    public void setClassLevel(Integer classLevel) {
        this.classLevel = classLevel;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassText() {
        return classText;
    }

    public void setClassText(String classText) {
        this.classText = classText;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getPriceText() {
        return priceText;
    }

    public void setPriceText(String priceText) {
        this.priceText = priceText;
    }

    public Float getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(Float waitingTime) {
        this.waitingTime = waitingTime;
    }

}
