package ru.vasidmi.comparetaxi.models.Klass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Tariff {

    @SerializedName("Cost")
    @Expose
    private Integer cost;
    @SerializedName("CostFull")
    @Expose
    private Float costFull;
    @SerializedName("IsMinimumCost")
    @Expose
    private Boolean isMinimumCost;
    @SerializedName("Cars")
    @Expose
    private Integer cars;
    @SerializedName("Discounts")
    @Expose
    private List<String> discounts = null;
    @SerializedName("Details")
    @Expose
    private List<String> details = null;
    @SerializedName("PrepaymentRequired")
    @Expose
    private Boolean prepaymentRequired;
    @SerializedName("CostCalculationId")
    @Expose
    private Integer costCalculationId;
    @SerializedName("Currency")
    @Expose
    private String currency;
    @SerializedName("Props")
    @Expose
    private Props props;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Name")
    @Expose
    private String name;

    /**
     * No args constructor for use in serialization
     */
    public Tariff() {
    }

    /**
     * @param details
     * @param cars
     * @param name
     * @param discounts
     * @param costFull
     * @param costCalculationId
     * @param prepaymentRequired
     * @param iD
     * @param isMinimumCost
     * @param cost
     * @param props
     * @param currency
     */
    public Tariff(Integer cost, Float costFull, Boolean isMinimumCost, Integer cars, List<String> discounts, List<String> details, Boolean prepaymentRequired, Integer costCalculationId, String currency, Props props, Integer iD, String name) {
        super();
        this.cost = cost;
        this.costFull = costFull;
        this.isMinimumCost = isMinimumCost;
        this.cars = cars;
        this.discounts = discounts;
        this.details = details;
        this.prepaymentRequired = prepaymentRequired;
        this.costCalculationId = costCalculationId;
        this.currency = currency;
        this.props = props;
        this.iD = iD;
        this.name = name;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Float getCostFull() {
        return costFull;
    }

    public void setCostFull(Float costFull) {
        this.costFull = costFull;
    }

    public Boolean getIsMinimumCost() {
        return isMinimumCost;
    }

    public void setIsMinimumCost(Boolean isMinimumCost) {
        this.isMinimumCost = isMinimumCost;
    }

    public Integer getCars() {
        return cars;
    }

    public void setCars(Integer cars) {
        this.cars = cars;
    }

    public List<String> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<String> discounts) {
        this.discounts = discounts;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    public Boolean getPrepaymentRequired() {
        return prepaymentRequired;
    }

    public void setPrepaymentRequired(Boolean prepaymentRequired) {
        this.prepaymentRequired = prepaymentRequired;
    }

    public Integer getCostCalculationId() {
        return costCalculationId;
    }

    public void setCostCalculationId(Integer costCalculationId) {
        this.costCalculationId = costCalculationId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Props getProps() {
        return props;
    }

    public void setProps(Props props) {
        this.props = props;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
