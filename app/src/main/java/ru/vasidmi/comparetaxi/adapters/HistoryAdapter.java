package ru.vasidmi.comparetaxi.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vasidmi.comparetaxi.R;
import ru.vasidmi.comparetaxi.TaxiForHistoryActivity;
import ru.vasidmi.comparetaxi.models.Route;
import ru.vasidmi.comparetaxi.models.SearchEntity;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private Context mContext;
    private List<SearchEntity> items;

    public HistoryAdapter(Context mContext, List<SearchEntity> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.history_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.from.setText("Откуда: " + items.get(position).getStartTitle());
        holder.to.setText("Куда: " + items.get(position).getEndTitle());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.route_image)
        AppCompatImageView routeImageView;
        @BindView(R.id.from)
        AppCompatTextView from;
        @BindView(R.id.to)
        AppCompatTextView to;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Route mRoute = new Route();
            double startLat, startLng, endLat, endLng;
            startLat = items.get(getAdapterPosition()).getStartLat();
            startLng = items.get(getAdapterPosition()).getStartLng();
            endLat = items.get(getAdapterPosition()).getEndLat();
            endLng = items.get(getAdapterPosition()).getEndLng();

            mRoute.setStartCoords(new LatLng(startLat, startLng));
            mRoute.setEndCoords(new LatLng(endLat, endLng));
            Intent mIntent = new Intent(mContext, TaxiForHistoryActivity.class);
            mIntent.putExtra("route", mRoute);
            mContext.startActivity(mIntent);
        }
    }
}

