package ru.vasidmi.comparetaxi.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vasidmi.comparetaxi.GlideApp;
import ru.vasidmi.comparetaxi.R;
import ru.vasidmi.comparetaxi.models.Taxi;

public class TaxiAdapter extends RecyclerView.Adapter<TaxiAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Taxi> items;
    private static final String YANDEX = "yandex";
    private static final String UBER = "uber";
    private static final String RITM = "ritm";
    private static final String KLASS = "klass";

    public TaxiAdapter(Context mContext, ArrayList<Taxi> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.taxi_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Drawable companyImage = null;
        switch (items.get(position).getCompany()) {
            case YANDEX:
                companyImage = mContext.getDrawable(R.drawable.yandex);
                break;
            case UBER:
                companyImage = mContext.getDrawable(R.drawable.uber);
                break;
            case RITM:
                companyImage = mContext.getDrawable(R.drawable.ritm);
                break;
            case KLASS:
                companyImage = mContext.getDrawable(R.drawable.klass);
                break;
        }
        GlideApp.with(mContext)
                .load(companyImage)
                .centerCrop()
                .circleCrop()
                .into(holder.taxiImage);
        holder.taxiTitle.setText(items.get(position).getTitle());
        holder.comment1.setText(items.get(position).getComment1());
        holder.taxiPrice.setText(String.format("≈ %d \u20BD", items.get(position).getPrice()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addTaxi(Taxi taxi) {
        this.items.add(0, taxi);
        notifyItemInserted(0);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.taxi_image)
        AppCompatImageView taxiImage;
        @BindView(R.id.taxi_title)
        AppCompatTextView taxiTitle;
        @BindView(R.id.taxi_price)
        AppCompatTextView taxiPrice;
        @BindView(R.id.comment1)
        AppCompatTextView comment1;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
