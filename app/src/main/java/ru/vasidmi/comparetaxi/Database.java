package ru.vasidmi.comparetaxi;

import android.app.Activity;

import java.util.List;

import io.objectbox.Box;
import ru.vasidmi.comparetaxi.models.SearchEntity;

public class Database {

    private Activity mActivity;

    public Database(Activity activity) {
        this.mActivity = activity;
    }

    public List<SearchEntity> getSearchValues() {
        Box<SearchEntity> search = ((App) mActivity.getApplication()).getBoxStore().boxFor(SearchEntity.class);
        return search.getAll();
    }

    public void addSearchValue(double startLat, double startLng,
                               double endLat, double endLng,
                               String startTitle,
                               String endTitle) {
        Box<SearchEntity> search = ((App) mActivity.getApplication()).getBoxStore().boxFor(SearchEntity.class);
        search.put(new SearchEntity(startLat, startLng, endLat, endLng, startTitle, endTitle));
    }
}
