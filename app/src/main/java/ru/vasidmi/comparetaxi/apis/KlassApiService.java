package ru.vasidmi.comparetaxi.apis;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.vasidmi.comparetaxi.models.Klass.KlassModel;
import ru.vasidmi.comparetaxi.models.Klass.TokenModel;

public interface KlassApiService {

    @GET("webapi")
    Call<KlassModel> getKlass(@Query("q") String q,
                              @Query("country0") String country0,
                              @Query("city0") String city0,
                              @Query("street0") String street0,
                              @Query("precision0") String precision0,
                              @Query("country1") String country1,
                              @Query("city1") String city1,
                              @Query("street1") String street1,
                              @Query("precision1") String precision1,
                              @Query("lang") String lang,
                              @Query("apikey") String apikey,
                              @Query("stoken") String stoken);

    @GET("webapi")
    Call<TokenModel> getToken(@Query("q") String q,
                              @Query("lang") String lang,
                              @Query("apikey") String apikey);


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://sedi.ru/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
