package ru.vasidmi.comparetaxi.apis;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import ru.vasidmi.comparetaxi.models.Ritm.RitmModel;

public interface RitmApiService {
    @Multipart
    @POST("/")
    Call<RitmModel> getRitm(@PartMap Map<String, RequestBody> params);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.taxi-ritm.ru/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
