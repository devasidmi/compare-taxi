package ru.vasidmi.comparetaxi.apis;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import ru.vasidmi.comparetaxi.models.Uber.UberModel;

public interface UberApiService {

    @GET("estimates/price")
    Call<UberModel> getUber(@Header("Authorization") String auth,
                            @Query("start_latitude") double startLatitude,
                            @Query("start_longitude") double startLongitude,
                            @Query("end_latitude") double endLatitude,
                            @Query("end_longitude") double endLongitude);


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.uber.com/v1.2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
