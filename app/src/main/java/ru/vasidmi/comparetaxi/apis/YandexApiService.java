package ru.vasidmi.comparetaxi.apis;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import ru.vasidmi.comparetaxi.models.Yandex.YandexModel;

public interface YandexApiService {


    @GET("taxi_info")
    Call<YandexModel> getYandex(@Query(value = "clid") String clid,
                                @Header("YaTaxi-Api-Key") String apiKey,
                                @Query("rll") String rll,
                                @Query("class") String class_);


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://taxi-routeinfo.taxi.yandex.net/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
