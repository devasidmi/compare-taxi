package ru.vasidmi.comparetaxi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.commons.lang.SerializationUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.vasidmi.comparetaxi.adapters.PlaceAdapter;
import ru.vasidmi.comparetaxi.models.Route;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.from)
    AutoCompleteTextView fromAutoCompleteTextView;
    @BindView(R.id.to)
    AutoCompleteTextView toAutoCompleteTextView;
    private MapFragment mapFragment;
    private LocationManager locationManager;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private PlaceAdapter mPlaceAdapter;
    private Context mContext;
    private Route mRoute;
    private boolean SHOWHIDE_MENU = false;
    private MaterialDialog loadingDialog, routingDialog;
    private Database mDatabase;

    private static final String LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String LOCATION_PERMISSION2 = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String SAVE_DATA = Manifest.permission.WRITE_EXTERNAL_STORAGE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        mDatabase = new Database(this);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mRoute = new Route();
        askPermissions();
    }

    private void askPermissions() {
        String[] permissions = {LOCATION_PERMISSION, LOCATION_PERMISSION2, SAVE_DATA};
        this.requestPermissions(permissions, 1);
    }

    private void showMenu() {
        SHOWHIDE_MENU = true;
        invalidateOptionsMenu();
    }

    private void hideMenu() {
        SHOWHIDE_MENU = false;
        invalidateOptionsMenu();
    }

    private void showLoadingDialog() {
        loadingDialog = new MaterialDialog.Builder(mContext)
                .progress(true, 0)
                .content("Загружаю данные...")
                .cancelable(false)
                .build();
        loadingDialog.show();
    }

    private void showRoutingDialog() {
        routingDialog = new MaterialDialog.Builder(mContext)
                .progress(true, 0)
                .content("Строю маршрут...")
                .cancelable(false)
                .build();
        routingDialog.show();
    }

    private void hideLoadingDialog() {
        if (loadingDialog != null && !loadingDialog.isCancelled()) {
            loadingDialog.dismiss();
        }
    }

    private void hideRoutingDialog() {
        if (routingDialog != null && !routingDialog.isCancelled()) {
            routingDialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == 0 && grantResults[1] == 0) {
            mapFragment.getMapAsync(this);
        }
    }

    private void askUserLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            askPermissions();
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 400, 100, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                hideLoadingDialog();
                Log.e("YAY!", "location changed!");
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                mGoogleMap.animateCamera(cameraUpdate);
                locationManager.removeUpdates(this);
                mPlaceAdapter = new PlaceAdapter(mContext, Places.getGeoDataClient(mContext), LatLngBounds.builder().include(latLng).build(), null);
                fromAutoCompleteTextView.setAdapter(mPlaceAdapter);
                toAutoCompleteTextView.setAdapter(mPlaceAdapter);
                fromAutoCompleteTextView.setOnItemClickListener(onSuggestedPlaceStartClickListener);
                toAutoCompleteTextView.setOnItemClickListener(onSuggestedPlaceEndClickListener);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        });
    }

    @OnClick(R.id.create_route)
    void createRoute() {
        if (mGoogleMap != null)
            mGoogleMap.clear();
        showRoutingDialog();
        final Routing routing = new Routing.Builder()
                .waypoints(mRoute.getStartCoords(), mRoute.getEndCoords())
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(new RoutingListener() {
                    @Override
                    public void onRoutingFailure(RouteException e) {
                        hideRoutingDialog();
                        Toast.makeText(mContext, "Ошибка построения маршрута", Toast.LENGTH_SHORT).show();
                        Log.e("YAY!", "onRoutingFailure");
                    }

                    @Override
                    public void onRoutingStart() {
                        Log.e("YAY!", "onRoutingStart");
                        routingDialog.show();
                    }

                    @Override
                    public void onRoutingSuccess(ArrayList<com.directions.route.Route> route, int shortestRouteIndex) {
                        showMenu();
                        hideRoutingDialog();
                        for (int k = 0; k < route.size(); ++k) {
                            PolylineOptions polyOptions = new PolylineOptions();
                            polyOptions.width(5);
                            polyOptions.addAll(route.get(k).getPoints());
                            mGoogleMap.addPolyline(polyOptions);
                        }
                        mDatabase.addSearchValue(mRoute.getStartLat(), mRoute.getStartLng(),
                                mRoute.getEndLat(), mRoute.getEndLng(),
                                mRoute.getStartTitle(), mRoute.getEndTitle());
                        mGoogleMap.addMarker(new MarkerOptions().position(mRoute.getStartCoords()));
                        mGoogleMap.addMarker(new MarkerOptions().position(mRoute.getEndCoords()));
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mRoute.getStartCoords(), 10));
                        invalidateOptionsMenu();
                    }

                    @Override
                    public void onRoutingCancelled() {
                        Log.e("YAY!", "onRoutingCancelled");
                    }
                })
                .waypoints(mRoute.getStartCoords(), mRoute.getEndCoords())
                .build();
        routing.execute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            askPermissions();
        }
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        showLoadingDialog();
        askUserLocation();
    }

    private AdapterView.OnItemClickListener onSuggestedPlaceStartClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            hideKeyBoard();
            AutocompletePrediction item = mPlaceAdapter.getItem(i);
            final PendingResult<PlaceBuffer> placeBufferPendingResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, item.getPlaceId());
            placeBufferPendingResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                @Override
                public void onResult(@NonNull PlaceBuffer places) {
                    if (!(places.getStatus().isSuccess())) {
                        Log.e("YAY!", "query error");
                        places.release();
                        return;
                    }
                    Place mPlace = places.get(0);
                    mRoute.setStartCoords(mPlace.getLatLng());
                    mRoute.setStartTitle(mPlace.getName().toString());
                    places.release();
                    hideMenu();
                }
            });
        }
    };

    private AdapterView.OnItemClickListener onSuggestedPlaceEndClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            hideKeyBoard();
            AutocompletePrediction item = mPlaceAdapter.getItem(i);
            final PendingResult<PlaceBuffer> placeBufferPendingResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, item.getPlaceId());
            placeBufferPendingResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                @Override
                public void onResult(@NonNull PlaceBuffer places) {
                    if (!(places.getStatus().isSuccess())) {
                        Log.e("YAY!", "query error");
                        places.release();
                        return;
                    }
                    Place mPlace = places.get(0);
                    mRoute.setEndCoords(mPlace.getLatLng());
                    mRoute.setEndTitle(mPlace.getName().toString());
                    places.release();
                    hideMenu();
                }
            });
        }
    };

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("YAY!", "onConnectionFailed");
        Toast.makeText(mContext, "Ошибка подключения", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        MenuItem taxiItem = menu.findItem(R.id.taxi_menu);
        MenuItem taxiHistory = menu.findItem(R.id.taxi_history);
        taxiItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent mIntent = new Intent(mContext, TaxiActivity.class);
                mIntent.putExtra("route", SerializationUtils.serialize(mRoute));
                startActivity(mIntent);
                return true;
            }
        });
        taxiHistory.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(mContext, HistoryActivity.class));
                return true;
            }
        });
        if (!SHOWHIDE_MENU) {
            taxiItem.setVisible(false);
            taxiHistory.setVisible(true);
        } else {
            taxiHistory.setVisible(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(fromAutoCompleteTextView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(toAutoCompleteTextView.getWindowToken(), 0);
    }
}
