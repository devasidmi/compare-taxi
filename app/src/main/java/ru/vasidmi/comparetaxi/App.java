package ru.vasidmi.comparetaxi;

import android.app.Application;

import io.objectbox.BoxStore;
import ru.vasidmi.comparetaxi.models.MyObjectBox;

public class App extends Application {

    public BoxStore mBoxStore;

    @Override
    public void onCreate() {
        super.onCreate();
        mBoxStore = MyObjectBox.builder().androidContext(App.this).build();
    }

    public BoxStore getBoxStore() {
        return mBoxStore;
    }
}
