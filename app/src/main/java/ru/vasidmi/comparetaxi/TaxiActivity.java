package ru.vasidmi.comparetaxi;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.apache.commons.lang.SerializationUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;
import ru.vasidmi.comparetaxi.fragments.HistoryFragment;
import ru.vasidmi.comparetaxi.fragments.TaxiNearFragment;
import ru.vasidmi.comparetaxi.models.Route;

public class TaxiActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.tabs)
    MaterialTabHost materialTabHost;


    private Route mRoute;
    private Context mContext;
    private String[] titles = {"РЯДОМ", "ИСТОРИЯ"};

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taxi);
        mContext = this;
        mRoute = (Route) SerializationUtils.deserialize((byte[]) getIntent().getExtras().get("route"));
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        for (int i = 0; i < 2; i++) {
            materialTabHost.addTab(
                    materialTabHost.newTab()
                            .setText(titles[i])
                            .setTabListener(new MaterialTabListener() {
                                @Override
                                public void onTabSelected(MaterialTab tab) {
                                    mViewPager.setCurrentItem(tab.getPosition());
                                }

                                @Override
                                public void onTabReselected(MaterialTab tab) {

                                }

                                @Override
                                public void onTabUnselected(MaterialTab tab) {

                                }
                            })
            );
            mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    materialTabHost.setSelectedNavigationItem(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return TaxiNearFragment.newInstance(mRoute);
                case 1:
                    return new HistoryFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
